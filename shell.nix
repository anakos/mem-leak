let
  pinnedVersion =
    builtins.fromJSON (builtins.readFile ./.nixpkgs-version.json);
  enableMozillaOverlay =
    true;
  mozilla-overlay =
    import
    (
      builtins.fetchTarball
      https://github.com/mozilla/nixpkgs-mozilla/archive/master.tar.gz
    );
  cfg = 
    {
      config.allowUnfree = true;
      overlays           = [ mozilla-overlay ];
    };
  pinnedPkgs =
    import (builtins.fetchGit {
      inherit (pinnedVersion) url rev;
      ref = "nixos-unstable";
    }) cfg;
  
in

{ pkgs ? pinnedPkgs }:

with pkgs; mkShell {
  RUST_BACKTRACE="full";
#  RUSTFLAGS="-g";
  RUST_SRC_PATH  = "${rustChannels.stable.rust-src}/lib/rustlib/src/rust/src";
  RUST_DOCS      = "${rustChannels.stable.rust-docs}/share/doc/rust/html/index.html";
#  HEAP_PROFILE_TIME_INTERVAL="10";
#  HEAP_PROFILE_INUSE_INTERVAL="524288";
#  HEAPCHECK="normal";
#  HEAP_CHECK_MAX_LEAKS="0";
#  HEAP_CHECK_IDENTIFY_LEAKS="true";
#  HEAP_PROFILE_MMAP="true";
  # TODO: why is SSL_CERT_FILE required in order to get cargo to work?
  SSL_CERT_DIR  = "/etc/ssl/certs";
  SSL_CERT_FILE = "/etc/ssl/certs/ca-certificates.crt";
  nativeBuildInputs = [
    # environment
    binutils
    gcc
    gnumake
    openssl
    pkgconfig
    git
    cacert
    pprof
    gperftools
    llvm
    graphviz
    libunwind
  ];
  
  buildInputs = [
    # environment
    binutils
    gcc
    gnumake
    openssl
    pkgconfig
    git
    gnuplot
    wrk2
    apacheHttpd
    docker-compose
    # profiling
    ltrace
    flamegraph
    linuxPackages.perf
    lsof
    valgrind
    massif-visualizer
    pprof
    gperftools
    protobuf
    go
    llvm
    libunwind
    # rust
    rustChannels.stable.rust
    rustChannels.stable.rust-src
    rustChannels.stable.rust-docs
    rustup
  ] ++
    pkgs.stdenv.lib.optionals pkgs.stdenv.isDarwin [
      pkgs.darwin.cf-private
      pkgs.darwin.Security
      pkgs.darwin.apple_sdk.frameworks.CoreServices
  ];
  shellHook = ''
    export LIB_TCMALLOC="${gperftools}/lib/libtcmalloc.so";
    export CARGO_HOME="$(pwd)/.cargo";
    export RUSTUP_HOME="$(pwd)/.rustup";
    export PROTOC="${protobuf}/bin/protoc";
  ''; 
}
