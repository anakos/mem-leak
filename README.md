# mem-leak

this exists because i am trying to figure out why my proxy is using so much memory.

to replicate:

1. start any http service on port 80. 
i.e. - docker run --rm -p 3000:80 nginx:latest
2. hit the proxy with requests from wrk2:
 wrk2 -c 150 -t 10 -d 15s --rate 1250 --latency 'http://0.0.0.0:9000/index.html'
 
Memory usage jumps based on the number of connections and never really drops back down.
