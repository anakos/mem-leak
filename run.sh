#!/usr/bin/env bash

watch -n10 -- wrk2 -c 150 -t 10 -d 15s --rate 1250 --latency 'http://0.0.0.0:9000/index.html'
