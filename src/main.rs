use {
    async_std::{io::copy, net::{TcpListener, TcpStream}, prelude::*, task,},
    futures_codec::{FramedRead, FramedWrite, BytesCodec},
    futures::{try_join,FutureExt,SinkExt},
    std::{
        net::SocketAddr,
        sync::Arc,
        ops::*
    },
};
pub type GenericError = Box<dyn std::error::Error + Send + Sync>;

#[async_std::main]
async fn main() -> Result<(), GenericError> {
    let proxy_addr  = "0.0.0.0:9000".parse::<SocketAddr>()?;
    let target_addr = "0.0.0.0:3000".parse::<SocketAddr>()?;

    task::block_on(accept_loop(proxy_addr, target_addr))?;

    Ok(())
}

async fn accept_loop(
    proxy_addr: SocketAddr,
    target_addr: SocketAddr
) -> Result<(), GenericError> {
    let listener = TcpListener::bind(proxy_addr).await?;
    
    let mut incoming = listener.incoming();
    while let Some(client_stream) = incoming.next().await {
        let client_stream = client_stream?;
        task::spawn(transfer_2(client_stream, target_addr));
    }

    drop(incoming);    
    drop(listener);

    Ok(())
}

async fn transfer(client_stream: TcpStream, target_addr: SocketAddr) {
    let outbound = match TcpStream::connect(target_addr).await {
        Ok(outbound) => outbound,
        Err(e)       => {
            println!("could not establish connection to downstream: {}", e);
            return;
        }
    };

    let (mut ri, mut wi) = &mut (&client_stream, &client_stream);
    let (mut ro, mut wo) = &mut (&outbound, &outbound);

    let client_to_server = copy(&mut ri, &mut wo);
    let server_to_client = copy(&mut ro, &mut wi);

    if let Err(e) = try_join!(client_to_server, server_to_client) {
        println!("an error occurred during transfer: {}", e);
    }

    if let Err(e) = client_stream.shutdown(std::net::Shutdown::Both) {
        println!("there was an error shutting down the client stream: {}", e);
    }

    if let Err(e) = outbound.shutdown(std::net::Shutdown::Both) {
        println!("there was an error shutting down the outbound stream: {}", e);
    }

    println!("transfer complete!");
}

async fn transfer_2(origin: TcpStream, target_addr: SocketAddr) {
    let downstream = match TcpStream::connect(target_addr).await {
        Ok(outbound) => outbound,
        Err(e)       => {
            println!("could not establish connection to downstream: {}", e);
            return;
        }
    };

    let origin = Arc::new(origin);
    let downstream = Arc::new(downstream);
    
    let origin_reader: &TcpStream = &origin;
    let origin_writer: &mut &TcpStream = &mut &*origin;
    let downstream_reader: &TcpStream = &downstream;
    let downstream_writer: &mut &TcpStream = &mut &*downstream; 

    let mut ri   = FramedRead::new(origin_reader, BytesCodec);
    let mut wo   = FramedWrite::new(downstream_writer, BytesCodec);
    let mut ro   = FramedRead::new(downstream_reader, BytesCodec);
    let mut wi   = FramedWrite::new(origin_writer, BytesCodec);
    
    let client_to_server = wo.send_all(&mut ri).boxed();
    let server_to_client = wi.send_all(&mut ro).boxed();

    if let Err(e) = try_join!(client_to_server, server_to_client) {
        println!("there was an error performing copy: {}", e);
    }
    
    if let Err(e) = origin.shutdown(std::net::Shutdown::Both) {
        println!("there was an error shutting down the client stream: {}", e);
    }

    if let Err(e) = downstream.shutdown(std::net::Shutdown::Both) {
        println!("there was an error shutting down the outbound stream: {}", e);
    }    
    
    println!("transfer complete! - dropped underlying connections");
}
